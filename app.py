from flask import Flask
from redis import Redis
import os
import socket

app = Flask(__name__)
redis = Redis(host='redis', port=6379)
host = socket.gethostname()

@app.route('/')
def hello():
    redis.incr('hits')
    return '<p>Hello Docker Employees!<p>Reload count: {} times.<p>The hostname that served this page was: {}'.format(redis.get('hits').decode("utf-8") ,host)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
